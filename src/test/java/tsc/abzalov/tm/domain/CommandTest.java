package tsc.abzalov.tm.domain;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class CommandTest {

    @Test
    @DisplayName("Test String Interpretation of Command")
    void stringInterpretationTest() {
        final Command command = new Command();
        assertEquals("", command.toString());

        command.setName(null);
        command.setArg(null);
        command.setDescription(null);
        assertEquals("", command.toString());

        command.setName("Test");
        assertEquals("Test", command.toString());

        command.setArg("-t");
        assertEquals("Test [-t]", command.toString());

        command.setDescription("Some Test Description");
        assertEquals("Test [-t]: Some Test Description", command.toString());

        command.setName(null);
        assertEquals("[-t]: Some Test Description", command.toString());

        command.setDescription(null);
        assertEquals("[-t]", command.toString());

        command.setName("Test");
        command.setArg(null);
        command.setDescription("Some Test Description");
        assertEquals("Test: Some Test Description", command.toString());

        command.setName(null);
        assertEquals("Some Test Description", command.toString());
    }

}