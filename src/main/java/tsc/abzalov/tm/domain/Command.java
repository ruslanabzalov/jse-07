package tsc.abzalov.tm.domain;

import static tsc.abzalov.tm.constant.ApplicationArgumentConst.*;
import static tsc.abzalov.tm.constant.ApplicationCommandConst.*;

public class Command {

    public static final Command HELP = new Command(
            CMD_HELP, ARG_HELP, "Shows all available commands."
    );

    public static final Command INFO = new Command(
            CMD_INFO, ARG_INFO, "Shows CPU and RAM info."
    );

    public static final Command ABOUT = new Command(
            CMD_ABOUT, ARG_ABOUT, "Shows developer info."
    );

    public static final Command VERSION = new Command(
            CMD_VERSION, ARG_VERSION, "Shows application version."
    );

    public static final Command EXIT = new Command(
            CMD_EXIT, null, "Shutdowns application."
    );

    private String name = "";
    private String arg = "";
    private String description = "";

    public Command() {
    }

    public Command(String name, String arg, String description) {
        this.name = name;
        this.arg = arg;
        this.description = description;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArg() {
        return this.arg;
    }

    public void setArg(String arg) {
        this.arg = arg;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        final String correctName = (this.name == null || this.name.isEmpty()) ? "" : this.name;
        final String correctArg = (this.arg == null || this.arg.isEmpty())
                ? "" : (!correctName.isEmpty())
                ? " [" + this.arg + "]" : "[" + this.arg + "]";
        final String correctDescription = (this.description == null || this.description.isEmpty())
                ? "" : (!correctArg.isEmpty() || !correctName.isEmpty())
                ? ": " + this.description + "" : this.description;
        return correctName + correctArg + correctDescription;
    }

}
