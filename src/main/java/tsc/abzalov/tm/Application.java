package tsc.abzalov.tm;

import java.util.Scanner;

import static tsc.abzalov.tm.constant.ApplicationArgumentConst.*;
import static tsc.abzalov.tm.constant.ApplicationCommandConst.*;
import static tsc.abzalov.tm.domain.Command.*;
import static tsc.abzalov.tm.util.Formatter.formatBytes;

public class Application {

    public static void main(String... args) {
        System.out.println();
        if (areArgExists(args)) return;

        displayWelcomeMessage();

        final Scanner scanner = new Scanner(System.in);
        String command;
        while (true) {
            System.out.print("Please, enter your command: ");
            command = scanner.nextLine();
            System.out.println();
            if (command == null || command.isEmpty()) continue;
            executeCommand(command);
        }
    }

    private static void displayWelcomeMessage() {
        System.out.println("****** WELCOME TO TASK MANAGER APPLICATION ******\n");
    }

    private static boolean areArgExists(String... args) {
        if (args == null || args.length == 0) return false;

        final String arg = args[0];
        if (arg == null || arg.isEmpty()) return false;

        processArg(arg);
        return true;
    }

    private static void processArg(String arg) {
        switch (arg) {
            case ARG_HELP:
                showHelp();
                break;
            case ARG_INFO:
                showInfo();
                break;
            case ARG_ABOUT:
                showAbout();
                break;
            case ARG_VERSION:
                showVersion();
                break;
            default:
                showError(arg, true);
        }
    }

    private static void executeCommand(String command) {
        switch (command) {
            case CMD_HELP:
                showHelp();
                break;
            case CMD_INFO:
                showInfo();
                break;
            case CMD_ABOUT:
                showAbout();
                break;
            case CMD_VERSION:
                showVersion();
                break;
            case CMD_EXIT:
                exit();
            default:
                showError(command, false);
        }
    }

    private static void showError(String input, boolean isArg) {
        if (isArg)
            System.out.println("Argument \"" + input + "\" is not available!\n" + "Please, use \"-h\" argument.\n");
        else
            System.out.println("Command \"" + input + "\" is not available!\n" + "Please, use \"help\" command.\n");
    }

    private static void showHelp() {
        System.out.println(HELP + "\n" + INFO + "\n" + ABOUT + "\n" + VERSION + "\n" + EXIT + "\n");
    }

    private static void showInfo() {
        final int cores = Runtime.getRuntime().availableProcessors();
        System.out.println("Cores: " + cores);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        System.out.println("Max Memory: " + formatBytes(maxMemory));

        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total Memory: " + formatBytes(totalMemory));

        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free Memory: " + formatBytes(freeMemory));

        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used Memory: " + formatBytes(usedMemory) + "\n");
    }

    private static void showAbout() {
        System.out.println("Developer Full Name: Ruslan Abzalov");
        System.out.println("Developer Email: rabzalov@tsconsulting.com\n");
    }

    private static void showVersion() {
        System.out.println("Version: 1.0.0\n");
    }

    private static void exit() {
        System.out.println("Application Is Closing...");
        System.exit(0);
    }

}
