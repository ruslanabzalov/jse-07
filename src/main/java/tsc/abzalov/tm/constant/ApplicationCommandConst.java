package tsc.abzalov.tm.constant;

public interface ApplicationCommandConst {

    String CMD_HELP = "help";

    String CMD_INFO = "info";

    String CMD_ABOUT = "about";

    String CMD_VERSION = "version";

    String CMD_EXIT = "exit";

}