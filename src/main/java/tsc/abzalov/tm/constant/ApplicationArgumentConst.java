package tsc.abzalov.tm.constant;

public interface ApplicationArgumentConst {

    String ARG_HELP = "-h";

    String ARG_INFO = "-i";

    String ARG_ABOUT = "-a";

    String ARG_VERSION = "-v";

}
